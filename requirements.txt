alembic~=1.7.7
fastapi~=0.75.1
pydantic~=1.9.0
python-pptx-templating~=0.0.3
python-multipart~=0.0.5
SQLAlchemy~=1.4.36
ujson~=5.2.0
uvicorn~=0.17.6