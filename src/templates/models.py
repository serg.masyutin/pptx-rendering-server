from sqlalchemy import Column, String

from db import Base


class Template(Base):
    __tablename__ = "template"

    name = Column(String(64), primary_key=True)
    file = Column(String(4032))
