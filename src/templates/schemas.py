from pathlib import Path
from typing import Optional

from pydantic import BaseModel


class Template(BaseModel):
    name: str
    file: Path
    # TODO list master slides
    # TODO list variables per master slide

    class Config:
        orm_mode = True


class Variable(BaseModel):
    name: str
    value: str
    autoformat: Optional[bool]
    key_value: Optional[bool]


class Slide(BaseModel):
    master: str
    variables: list[Variable]


class SlideDeck(BaseModel):
    template: str
    slides: list[Slide]
