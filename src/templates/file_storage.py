import os
import shutil
from pathlib import Path
from typing import Final

from fastapi import UploadFile, APIRouter, HTTPException

from config import get_settings

FILES_MOUNT_POINT: Final[str] = "/files"

router = APIRouter(
    prefix="/upload",
)


def templates_root() -> Path:
    return get_settings().templates_root


def local_path(path: Path) -> Path:
    return templates_root() / path


def check_file_exists(path: Path) -> bool:
    return os.path.isfile(local_path(path))


def local_path_if_exists(path: Path) -> Path:
    result = local_path(path)
    if os.path.isfile(result):
        return result

    raise FileNotFoundError(path)


def create_local_file(path: Path, file):
    local_file = local_path(path)
    with local_file.open(mode="wb") as f:
        shutil.copyfileobj(file, f)

    return path


@router.post("/{file_name}")
def upload_file(file_name: str, file: UploadFile):
    if check_file_exists(Path(file_name)):
        raise HTTPException(status_code=400, detail=f'File "{file_name}" exists')

    return FILES_MOUNT_POINT / create_local_file(Path(file_name), file.file)
