from fastapi import APIRouter, HTTPException, Depends
from sqlalchemy.orm import Session

from db import get_db
from templates import file_storage
from templates import models, schemas


def create(template: schemas.Template, db: Session) -> models.Template:
    new_template = models.Template(name=template.name, file=template.file)
    db.add(new_template)
    db.commit()
    db.refresh(new_template)
    return new_template


def lookup(name: str, db: Session) -> models.Template:
    return db.query(models.Template).filter_by(name=name).first()


def delete(template: schemas.Template, db: Session) -> models.Template:
    db.delete(template)
    db.commit()
    return template


router = APIRouter(
    prefix="/templates",
)


@router.post("/", response_model=schemas.Template)
def create_template(template: schemas.Template, db: Session = Depends(get_db)):
    existing_template = lookup(template.name, db)
    if existing_template:
        raise HTTPException(status_code=400, detail=f'Template "{template.name}" already exists')

    if not file_storage.check_file_exists(template.file):
        raise HTTPException(status_code=400, detail=f'File "{template.file}" does not exist')

    return create(template, db)


@router.get("/{name}", response_model=schemas.Template)
def get_template(name: str, db: Session = Depends(get_db)):
    result = lookup(name, db)
    if not result:
        raise HTTPException(status_code=404, detail=f'Template "{name}" not found')

    return result


@router.delete("/{name}", response_model=schemas.Template)
def delete_template(name: str, db: Session = Depends(get_db)):
    existing_template = lookup(name, db)
    if not existing_template:
        raise HTTPException(status_code=400, detail=f'Template "{name}" does not exists')

    return delete(existing_template, db)


@router.get("/", response_model=list[schemas.Template])
def list_templates(offset: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    return db.query(models.Template).offset(offset).limit(limit).all()
