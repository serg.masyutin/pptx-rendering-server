import os
import uuid
from pathlib import Path

import uvicorn
from fastapi import FastAPI, Depends
from fastapi.staticfiles import StaticFiles
from pptx import Presentation
from sqlalchemy.orm import Session
from starlette.background import BackgroundTasks
from starlette.responses import FileResponse
from templating.templating import add_slide

from config import get_settings
from db import get_db, engine
from templates import file_storage
from templates import template_repository
from templates import schemas

template_repository.models.Base.metadata.create_all(bind=engine)

app = FastAPI()
app.mount(file_storage.FILES_MOUNT_POINT, StaticFiles(directory=get_settings().templates_root))
app.include_router(file_storage.router)
app.include_router(template_repository.router)


def remove_temp_file(path: Path):
    os.remove(path)


def lookup_presentation(name: str, db: Session) -> Presentation:
    return Presentation(
        pptx=file_storage.local_path_if_exists(
            template_repository.lookup(name, db).file
        )
    )


@app.post("/render", response_class=FileResponse)
def render(deck: schemas.SlideDeck, background_tasks: BackgroundTasks, db: Session = Depends(get_db)):
    template_deck = lookup_presentation(deck.template, db)

    for slide in deck.slides:
        add_slide(
            template_deck,
            slide.master,
            {variable.name: variable.value for variable in slide.variables},
        )

    # FIXME use tempfile here
    result = get_settings().temporary_folder / f"{uuid.uuid4()}.pptx"
    template_deck.save(result)

    background_tasks.add_task(remove_temp_file, result)

    return result


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
