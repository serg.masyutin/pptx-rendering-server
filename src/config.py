import logging
from functools import lru_cache
from pathlib import Path

from pydantic import BaseSettings


class Settings(BaseSettings):
    env_name: str = "memory"
    db_url: str = "sqlite+pysqlite://"
    templates_root: Path = Path("./templates")
    temporary_folder: Path = Path("/tmp")

    class Config:
        env_file = ".env"


@lru_cache()
def get_settings() -> Settings:
    result = Settings()
    logging.info("Loaded settings environment %s", result.env_name)
    return result
